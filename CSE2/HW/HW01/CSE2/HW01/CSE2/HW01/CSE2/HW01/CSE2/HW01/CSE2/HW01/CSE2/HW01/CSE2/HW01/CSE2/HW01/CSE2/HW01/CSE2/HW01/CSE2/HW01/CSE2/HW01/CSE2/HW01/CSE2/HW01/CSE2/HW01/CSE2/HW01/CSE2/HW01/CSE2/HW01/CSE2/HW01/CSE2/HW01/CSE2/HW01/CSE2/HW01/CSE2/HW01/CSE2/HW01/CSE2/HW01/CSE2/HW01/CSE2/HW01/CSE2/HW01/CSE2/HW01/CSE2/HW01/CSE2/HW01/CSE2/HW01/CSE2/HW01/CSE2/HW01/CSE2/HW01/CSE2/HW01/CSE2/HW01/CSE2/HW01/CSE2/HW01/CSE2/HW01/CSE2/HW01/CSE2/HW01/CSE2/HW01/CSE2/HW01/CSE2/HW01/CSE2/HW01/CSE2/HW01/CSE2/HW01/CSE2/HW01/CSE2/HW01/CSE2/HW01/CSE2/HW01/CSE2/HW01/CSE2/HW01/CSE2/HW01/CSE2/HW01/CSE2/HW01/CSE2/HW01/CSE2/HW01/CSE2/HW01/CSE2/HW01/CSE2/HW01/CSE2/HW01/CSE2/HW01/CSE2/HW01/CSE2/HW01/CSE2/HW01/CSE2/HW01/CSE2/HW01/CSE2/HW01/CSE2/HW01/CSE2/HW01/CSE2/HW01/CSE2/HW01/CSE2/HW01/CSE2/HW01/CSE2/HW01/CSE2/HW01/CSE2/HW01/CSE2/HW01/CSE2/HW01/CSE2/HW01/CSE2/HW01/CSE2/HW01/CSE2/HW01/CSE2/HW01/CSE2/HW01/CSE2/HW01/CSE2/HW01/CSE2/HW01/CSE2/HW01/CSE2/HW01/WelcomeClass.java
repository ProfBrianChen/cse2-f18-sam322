//Sahil Malhotra
// 9/4/18
// CSE 002
// Homework 1: WelcomeClass
public class WelcomeClass {
  public static void main(String[] args){ 
   /*Needs to be printed to terminal 
  -----------
  | WELCOME |
  -----------
  ^  ^  ^  ^  ^  ^
 / \/ \/ \/ \/ \/ \
<-E--J--K--0--0--0->
 \ /\ /\ /\ /\ /\ /
  v  v  v  v  v  v
*/
    
  System.out.println("    -----------");
  System.out.println("    | WELCOME |");
  System.out.println("    -----------");
  System.out.println("  ^  ^  ^  ^  ^  ^");
  System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
  System.out.println("<-E--J--K--0--0--0->");
  System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println("  v  v  v  v  v  v");
  }
}