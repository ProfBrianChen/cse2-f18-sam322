//Sahil Malhotra
//CSE 002
//9-10-18
//Homework 2: Arithmetic Calculation

public class ArithmeticCalc{
  public static void main(String[] args){
 //Number of pairs of pants
   int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
    
//Calculations
//Total cost of each kind of item (i.e. total cost of pants, etc)
  double totalCostOfPants = numPants*pantsPrice;
//Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
  double saleTaxOnBelts = paSalesTax*beltCost;
 //Total cost of purchases (before tax)
  double totalCost=totalCostOfPants+ (numBelts*beltCost) + (numShirts*shirtPrice);
 //Total sales tax
 double saleTax= paSalesTax * totalCost;
 //Total paid for this transaction, including sales tax. 
 double totalPaidForTransaction = totalCost + saleTax;

    //Printing out statments
    /*
    Next, compute and display the cost of each of item type and the total sales tax paid for buying all of that item. 
    Finally, compute and display the total cost of the purchases (before tax), the total sales tax, and the total cost of the purchases (including sales tax). 
    */
System.out.println("The total cost for pants is " + totalCostOfPants + " and the sales tax on these items are " + (paSalesTax*numPants));//Cost of pants and tax
System.out.println("The total cost for Shirts is " + (numShirts*shirtPrice) + " and the sales tax is " + (paSalesTax*numShirts));// Cost of Shirts and tax
System.out.println("The total cost for Belts is " + (numBelts * beltCost) + " and the sales tax is " +(paSalesTax * numBelts));// Cost of Belt and tax
System.out.println("Total Cost of the purchases before Tax " + totalCost);
System.out.println("The total sales tax " + ((int)(100 * saleTax))/100.0);
System.out.println("Total Cost of the purchases including sales tax " + ((int) (100 * totalPaidForTransaction))/100.0);
   
                   

    
  }
}