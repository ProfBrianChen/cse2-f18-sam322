//Sahil Malhotra
//9-17-2018
//CSE 002
/*
Program #2. Write a program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid. Save the program in the file Pyramid.java. The measurements should be of type double.

Here are some sample runs:
The square side of the pyramid is (input length): 25
The height of the pyramid is (input height): 15
The volume inside the pyramid is: 3125.

*/
import java.util.Scanner; //Import statment
public class Pyramid{ //Class Open
  public static void main(String[] args){//Open Main
  Scanner scan=new Scanner(System.in);
  System.out.print("The square side of the pyramid is (input length)");
  double squareSide = scan.nextDouble();
  System.out.print("The height of the pyramid is");
  double height = scan.nextDouble();
  
  //Calculation
  //Formula= (1/3)BH
  double volume =((squareSide*height)/3); 
  System.out.println("Volume of a Pyramid is:" + volume);
 
  }//Close Main
  
  
}//Class Close