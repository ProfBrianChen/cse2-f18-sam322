//Sahil Malhotra
//9-17-2018
//CSE 002

/*
Prompt:
Program #1. Hurricanes drop a tremendous amount of water on areas of land.  
Write a program that asks the user for doubles that
represent the number of acres of land affected by hurricane precipitation 
and how many inches of rain were dropped on average. 
Convert the quantity of rain into cubic miles. 
Save the program as Convert.java

Expected Output:
Enter the affected area in acres: 23523.23
Enter the rainfall in the affected area: 45
0.02610444 cubic miles

*/

import java.util.Scanner;
public class Convert {
  public static void main(String[] args){
    Scanner scan= new Scanner(System.in);
    System.out.print("Enter the affected area in acres");
    double areaAcre = scan.nextDouble();
    System.out.print("Enter the rainfall in the affected area");
    double rainfall =scan.nextDouble();
    double acreInches = areaAcre * rainfall;
    double gallons = acreInches * 27154;
    double cubicMiles = gallons / 1101117147428.6;
    System.out.println( cubicMiles + " cubic miles");
    
  }
}