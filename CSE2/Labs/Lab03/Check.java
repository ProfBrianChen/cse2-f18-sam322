//Sahil Malhotra
//9-12-18
//CSE 002

/*
This program will allow the user to split a bill between several people by using the scanner class
*/
import java.util.Scanner; //To allow input from user
public class Check{//Class
  public static void main(String args[]){//Main Method
    Scanner Scan=new Scanner(System.in);
    System.out.print("Enter the original cost of the check in the form xx.xx");
    double checkCost = Scan.nextDouble();//Recieving cost of check
    System.out.println("What is your prefered tip percentage, in form xx");
    double tipPercent = Scan.nextDouble();//Recieving Tip Percent
    tipPercent /= 100; //Converting Tip percent to decimal value
    System.out.print("How many people will you be spliting the check with?");
    int numPeople = Scan.nextInt();
    
    //Calculation and Print Portion of Code
    double totalCost;
    double costPerPerson; 
    int dollars,  //Dollar is the whole dollar amount of the purchase
    dimes, //For storing values to the right of the decimal point
    pennies;//For storing values to the right of the decimal point
    totalCost = checkCost * (1+tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping the decimal
    dollars = (int)costPerPerson;
    dimes=(int)(costPerPerson * 10) % 10;//Calculating the dimes amount
    pennies=(int)(costPerPerson * 100) % 10;//Calculating the pennies amount
    System.out.println("Each person in the group owes $" + dollars + "."+ dimes+pennies);

    
    
    
  }//Main Method close
}//Class Close