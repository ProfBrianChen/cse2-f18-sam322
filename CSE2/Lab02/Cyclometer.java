//Sahil Malhotra
//9-5-18
//CSE 2 
/*
My bicycle cyclometer (meant to measure speed, distance, etc.) records two kinds of data, the time elapsed in seconds, 
and the number of rotations of the front wheel during that time. For two trips, given time and rotation counts

This program will: 
print the number of minutes for each trip
print the number of counts for each trip
print the distance of each trip in miles
print the distance for the two trips combined



*/
public class Cyclometer{ 
  //Main Method required for every Java Program
  public static void main(String[] args){
    int secsTrip1 = 480; //Number of seconds in trip 1
    int secsTrip2 = 3220; // Number of seconds in trip 2
    int countsTrip1 = 1516; // Number of rotations in trip 1
    int countsTrips2 = 9037; //Number of rotations in trip 2
    double wheelDiameter = 27.0, //Diameter of the Wheel
    PI = 3.14159, //Math PI
    feetPerMile = 5280, //Feet per miles
    inchesPerFoot =12, //Inches per Foot
    secondsPerMinute = 60; // seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; //Distance variables
    
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts");
    System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+countsTrips2+" counts");
    //Calculations
    //Gives distance in inches for each count, a rotation of a wheel travels diameter in inches times PI
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrips2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//Total distance in trip
    totalDistance=distanceTrip1+distanceTrip2;
    //Printing out Data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");

    

    
  }//End of main Method
}//End of class